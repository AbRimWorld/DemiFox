[h1]About the mod :[/h1]
Demi Fox is a mostly lite Humanoid Alien Race mod.
Demi Fox race members can be found within Vanilla factions, quests, raids, and a start scenario.
Demi Fox race members are physically weaker and more fragile than humans, but more psychically capable.
This mod does not add any factions or items.
Adding this mod mid save is fine, removing it is not.
Strongly recommended to use this mod with the Royalty DLC.
Using Anima Interaction on an anima tree with less than 5 anima grass will kill the tree.

[h1]Known / Possible Issues :[/h1]
[list]
[*]Vanilla body and head texture replacers will affect Demi Fox which can lead to ear and tail offSets not lining up correctly.
-So long as you do not use multiple of these at the same time an offSet patch can be made to solve the offSet issues, but only if I know about the problem bodies.

[*]Cosmetic Gene mods which add ears or tails or any other graphic may clash and result in displaying both the addon and the gene graphic.
-All I really can do about this is block said genes from appearing on pawns spawned further on.

[*]Pawns spawned before a fix is made may not automatically fix and manual editing may be needed in order to fix already existing pawns.

[*]Pawns gaining a pyslink level while also already having an ability of the new level before the upgrade will not gain any new abilities.
-This is Vanilla behavior.

[*][url=https://steamcommunity.com/sharedfiles/filedetails/?id=2017782001]Xeva FB Pack[/url] heads  have ears drawn on them.
-I do not have plans to fix this my self, a solution exists when using [url=https://steamcommunity.com/sharedfiles/filedetails/?id=2017782001]Xeva FB Pack[/url] along side [url=https://steamcommunity.com/sharedfiles/filedetails/?id=1635901197][NL] Facial Animation - WIP[/url].

[*][url=https://steamcommunity.com/sharedfiles/filedetails/?id=2017782001]Xeva FB Pack[/url] Hides Archotech eye graphics.
-I am still deciding on how to approach  this issue. Disabling the Archotech tail texture when [url=https://steamcommunity.com/sharedfiles/filedetails/?id=2017782001]Xeva FB Pack[/url] is detected is a simple enough task, but... the textures are pretty.
[/list]

[h1]Compatibility :[/h1]
[h3]Combat Extended[/h3]
[list]
[*]If combat extended is not feeling like being incompatible with HAR then it should be compatible with Demi Fox.
[/list]

[h3][url=https://steamcommunity.com/sharedfiles/filedetails/?id=2017782001]Xeva FB Pack[/url][/h3]
[list]
[*]Tail and Ear offsets are adjusted to match.

[*]Do not use this mod with [url=https://steamcommunity.com/sharedfiles/filedetails/?id=1906148376][NL] Compatible Body 2[/url].
[/list]

[h3][url=https://steamcommunity.com/sharedfiles/filedetails/?id=1635901197][NL] Facial Animation - WIP[/url][/h3]
[list]
[*]Ear offsets are adjusted to match.

[*]Uses an ear-less Facial Animation face.

[*]Babies use droopy ear variant to fit facial animation face.

[*]Works with [url=https://steamcommunity.com/sharedfiles/filedetails/?id=2017782001]Xeva FB Pack[/url].

[*]No Support for [url=https://steamcommunity.com/sharedfiles/filedetails/?id=1635901197][NL] Facial Animation - WIP[/url] retexture mods.
[/list]

[h3][url=https://steamcommunity.com/sharedfiles/filedetails/?id=1906148376][NL] Compatible Body 2[/url][/h3]
[list]
[*]Tail and Ear offsets are adjusted to match.

[*]Do not use this mod with [url=https://steamcommunity.com/sharedfiles/filedetails/?id=2017782001]Xeva FB Pack[/url].
[/list]

[h1]Notes :[/h1]
Changing graphics of ear or tail texture variant can be done via [url=https://steamcommunity.com/sharedfiles/filedetails/?id=1874644848]Character Editor[/url].

[h1]Credit :[/h1]
[list]
[*]Abraxas - Code and XML
[*][url=https://steamcommunity.com/id/NightmareCorporation/myworkshopfiles/?appid=294100]NightmareCorporation[/url] - Code and support
[*][url=https://steamcommunity.com/profiles/76561198182079259/myworkshopfiles/?appid=294100]ShauaPuta[/url] - Art
[*][url=https://steamcommunity.com/profiles/76561198057157623/myworkshopfiles/?appid=294100]VanillaSky[/url] - FacialAnimations earless texture
[*][url=https://steamcommunity.com/id/WRelicK/myworkshopfiles/?appid=294100]WRelicK[/url] - Text and Descriptions improvements.
[*][url=https://twitter.com/Anh2132/]Nguyenanh2132[/url] - Preview art
[*][url=https://steamcommunity.com/profiles/76561197992216829/myworkshopfiles/?appid=294100]Gouda quiche[/url] - Preview art text
[/list]


[h1]Links :[/h1]
[url=https://gitlab.com/AbRimWorld/DemiFox]Git[/url]

[h3]Like my work and want to support me?[/h3]
　　　　　　　　　　　[url=https://ko-fi.com/abraxas_][img]https://cdn.ko-fi.com/cdn/kofi5.png[/img][/url]